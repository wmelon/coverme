var filesystem = require('fs');

var singleton = function singleton(){
    var _path = "";
    var _router = null;
    var _orm = null;
    this.setup = function (path, router, orm){
        _path = path;
        _router = router;
        _orm = orm;
        if(_router === null || _orm === null || _path === null) {
            throw "Invalid Options.";
        } else {
            init();
        }
    }

    function init() {
        filesystem.readdirSync(_path).forEach(function(name){
            console.log("Setting up " + _path + "/" + name);
            var object = require(_path + "/" + name);
            object(_router,_orm);
        });
    }

    if(singleton.caller != singleton.getInstance){
        throw new Error("This object cannot be instanciated");
    }
}

singleton.instance = null;

singleton.getInstance = function(){
    if(this.instance === null){
        this.instance = new singleton();
    }
    return this.instance;
}

module.exports = singleton.getInstance();