module.exports = function(apiRouter, orm) {
	apiRouter.route("/users").post(function(req, res) {
		var user = orm.model("users");
		try {
			user.create({email: req.body.email, phone: req.body.phone}).success(function(user) {
				res.send(user.values);
			}).error(function(error) {
				res.send(error);
			});
		} catch(e) {
			res.send(e);
		}
		
		//res.send(200,req.body);
	}).get(function(req, res) {
		var user = orm.model("users");
		user.findAll().success(function(users) {
			res.send(users);
		});
	});

	apiRouter.route("/users/:id(\\d+)").get(function(req, res){
		if(parseInt(req.params.id) !== undefined){
			var user = orm.model("users");
			user.find({where: {id: req.params.id}}).success(function(user) {
				res.send(user);
			}).error(function(error) {
				res.send(error);
			});
		} else {
			res.send(404, "User Not Found");
		}
	}).put(function(req,res){
		if(parseInt(req.params.id) !== undefined){
			var user = orm.model("users");
			user.find({where: {id: req.params.id}}).success(function(user) {
				user.email = req.params.email;
				user.phone = req.params.phone;
				user.save().success(function() {
					res.send({status:"success"});
				});
			}).error(function(error) {
				res.send(error);
			});
		} else {
			res.send(404, "User Not Found");
		}	
	}).delete(function(req,res) {
		if(parseInt(req.params.id) !== undefined){
			var user = orm.model("users");
			user.find({where: {id: req.params.id}}).success(function(user) {
				user.destroy().success(function() {
					res.send({status:"success"});
				});
			}).error(function(error) {
				res.send(error);
			});
		} else {
			res.send(404, "User Not Found");
		}	
	});
}