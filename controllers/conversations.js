module.exports = function(apiRouter, orm) {
	apiRouter.route("/conversations").post(function(req, res) {
		var convo = orm.model("conversations");
		var user = orm.model("users");
		try {
			if(parseInt(req.body.userid) !== undefined){
				var users = orm.model("users");
				users.find({where: {id: req.body.userid}}).success(function(user) {
					convo.create().success(function(convo) {
						user.addConversation(convo).success(function() {
							res.send(convo.values);
						});
					}).error(function(error) {
						res.send(error);
					});
				}).error(function(error) {
					res.send(error);
				});
			} else {
				res.send(404, "User Not Found");
			}
		} catch(e) {
			res.send(e);
		}
		
		//res.send(200,req.body);
	}).get(function(req, res) {
		var user = orm.model("conversations");
		user.findAll({include: [orm.model("messages")]}).success(function(users) {
			res.send(users);
		});
	});

	apiRouter.route("/conversations/:id(\\d+)").get(function(req, res){
		if(parseInt(req.params.id) !== undefined){
			var convo = orm.model("conversations");
			convo.find({where: {id: req.params.id}, include: [orm.model("messages")]}).success(function(convo) {
				res.send(convo);
			}).error(function(error) {
				res.send(error);
			});
		} else {
			res.send(404, "User Not Found");
		}
	}).delete(function(req,res) {
		if(parseInt(req.params.id) !== undefined){
			var convo = orm.model("conversations");
			convo.find({where: {id: req.params.id}}).success(function(convo) {
				convo.destroy().success(function() {
					res.send({status:"success"});
				});
			}).error(function(error) {
				res.send(error);
			});
		} else {
			res.send(404, "User Not Found");
		}	
	});
}