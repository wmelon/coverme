module.exports = function(apiRouter, orm) {
	apiRouter.route("/messages").post(function(req, res) {
		var convo = orm.model("conversations");
		var user = orm.model("messages");
		try {
			if(parseInt(req.body.userid) !== undefined){
				var users = orm.model("users");
				users.find({where: {id: req.body.userid}}).success(function(user) {
					convo.create().success(function(convo) {
						user.addConversation(convo).success(function() {
							res.send(convo.values);
						});
					}).error(function(error) {
						res.send(error);
					});
				}).error(function(error) {
					res.send(error);
				});
			} else {
				res.send(404, "User Not Found");
			}
		} catch(e) {
			res.send(e);
		}
		
		//res.send(200,req.body);
	}).get(function(req, res) {
		var user = orm.model("messages");
		user.findAll().success(function(users) {
			res.send(users);
		});
	});

	apiRouter.route("/messages/:id(\\d+)").get(function(req, res){
		if(parseInt(req.params.id) !== undefined){
			var user = orm.model("users");
			user.find({where: {id: req.params.id}}).success(function(user) {
				res.send(user);
			}).error(function(error) {
				res.send(error);
			});
		} else {
			res.send(404, "User Not Found");
		}
	}).put(function(req,res){
		if(parseInt(req.params.id) !== undefined){
			var user = orm.model("conversations");
			user.find({where: {id: req.params.id}}).success(function(user) {
				user.email = req.params.email;
				user.phone = req.params.phone;
				user.save().success(function() {
					res.send({status:"success"});
				});
			}).error(function(error) {
				res.send(error);
			});
		} else {
			res.send(404, "User Not Found");
		}	
	}).delete(function(req,res) {
		if(parseInt(req.params.id) !== undefined){
			var user = orm.model("users");
			user.find({where: {id: req.params.id}}).success(function(user) {
				user.destroy().success(function() {
					res.send({status:"success"});
				});
			}).error(function(error) {
				res.send(error);
			});
		} else {
			res.send(404, "User Not Found");
		}	
	});
}