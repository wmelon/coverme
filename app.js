// Include Libraries
var express = require("express");
var bodyParser = require('body-parser');
var fs = require('fs');
var sequelize = require('sequelize');

var orm = require("./lib/model");
orm.setup('./models', "coverme", "covermeapp", "ac6RvREG", {
    host: 'localhost'
});

// Set up routing engine
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

var port = process.env.PORT || 8080;

// Standard File Router
var router = express.Router();

router.get("/", function(req, res) {
	// pass out main page
	res.sendfile("./index.html");
});

router.get("*", function(req, res) {
	fs.exists("./" + req.path, function(exists) {
		//console.log(req.path);
		if(exists && req.path != "/app.js") {
			res.sendfile("./" + req.path);
		} else {
			res.send(404, "Not Found");
		}
	});
});

// RESTful API router
var apiRouter = express.Router();

apiRouter.use(function(req,res,next) {
	console.log("Request for " + req.path);
	next();
});

require("./routes").setup("./controllers", apiRouter, orm);

app.use("/api", apiRouter);
app.use("/", router);



app.listen(port);
console.log("starting server on port " + port);