var orm = require("../lib/model"), Seq = orm.Seq();

module.exports = {
	model: {
		id: {type: Seq.BIGINT, primaryKey:true, autoIncrement: true},
		created: {type: Seq.DATE, defaultValue: Seq.NOW},
		sendDate: {type: Seq.DATE, defaultValue: Seq.NOW},
		sent: {type: Seq.BOOLEAN, allowNull: false, defaultValue: false},
		active: {type: Seq.BOOLEAN, allowNull:false, defaultValue: true}
	},
	relations: {
		belongsTo: "users",
		hasMany: "messages"	
	}
}
