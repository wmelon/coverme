var orm = require("../lib/model"), Seq = orm.Seq();

module.exports = {
	model: {
		id: {type: Seq.INTEGER, primaryKey: true, autoIncrement: true},
		email: {type: Seq.STRING, unique: true},
		phone: {type: Seq.STRING, unique: true},

		created: {type: Seq.DATE, defaultValue: Seq.NOW},
		phoneChanged: {type: Seq.DATE, defaultValue: Seq.NOW},
		active: {type: Seq.BOOLEAN, allowNull:false, defaultValue: true}
	},
	relations: {
		hasMany: "conversations"	
	}
}
