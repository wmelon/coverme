var orm = require("../lib/model"), Seq = orm.Seq();

module.exports = {
	model: {
		id: {type: Seq.BIGINT, primaryKey: true, autoIncrement: true},
		messageText: {type: Seq.STRING(140), allowNull: false, defaultValue: 'Halp'},
		incoming: {type: Seq.BOOLEAN, allowNull: false, defaultValue: false},
		active: {type: Seq.BOOLEAN, allowNull:false, defaultValue: true}
	},
	relations: {
		belongsTo: "conversations"
	}
}
