$(function() {

	var App = new (Backbone.View.extend({
		Models : {},
		Views: {},
		Collections: {},
		template: _.template('<div class="container"><div class="row"><div id="nav" class="col-sm-3"></div><div id="content" class="col-sm-9"></div></div></div>'),
		initialize: function () {

		},
		render: function () {
			this.$el.html(this.template());
			return this;
		},
		events: {
			'click #nav a': function(e) {
				e.preventDefault();
				Backbone.history.navigate(e.target.pathname, {trigger: true});
			}
		},
		start: function () {
			Backbone.history.start({pushState:true});
			Backbone.history.navigate("", {trigger: true});
			var nav1 = new App.Models.NavItem({name: "Home", path: "/", active: true});
			var nav2 = new App.Models.NavItem({name: "My Account", path: "/my-account", active:false});
			var navList = new App.Collections.NavItems({});
			navList.push(nav1);
			navList.push(nav2);
			var navLV = new App.Views.NavItems({collection: navList});
			$("#nav").html(navLV.render().el);
		}

	}))({el: document.body});


	// Models
	App.Models.NavItem = Backbone.Model.extend({
		defaults: function() {
			return {
				name: "",
				path: "",
				active: false
			}
		}
	});

	//App.Models.Conversation = new Backbone.Model.extend ({});


	// Views
	App.Views.NavItem = Backbone.View.extend({
		model: App.Models.NavItem,
		tagName: "li",
		template: _.template("<a href='<%= path %>'><%= name %></a>"),
		render: function () {
			if(this.model.get("active")) {
				this.$el.attr("class","active");
			}
			this.$el.html(this.template(this.model.attributes));

			return this;
		},
		events: {
			'click': function(e) {
				e.preventDefault();
				this.trigger('changeActive', this.path);
				Backbone.history.navigate(this.path, {trigger: true});
			},
			'changeActive': function(e) {
				if(e !== this.model.get(path)) {
					this.model.set('active', false);
				} else {
					this.mode.set('active', true);
				}
			}
		}
	});

	App.Views.NavItems = Backbone.View.extend({
		model: App.Collections.NavItems,
		_navItems: [],
		tagName: 'ul',
		className: "nav nav-pills nav-stacked",
		initialize: function() {
			var view = this;
			this.collection.on('add', this.render, this);
			this.collection.forEach(function(navItem) {
				view._navItems.push(new App.Views.NavItem({model: navItem}));
			});
		},
		render: function() {
			var view = this;
			this._navItems.forEach(function(nv) {
				view.$el.append(nv.render().el);
			});
			return this;
		}
	});

	//App.Views.Conversation = Backbone.View.extend({
	//	model: App.Models.Conversation
	//});


	// Collections
	App.Collections.NavItems = Backbone.Collection.extend({ model: App.Models.NavItem });

	//App.Collections.Conversations = Backbone.Collection.extend({ model: App.Models.Conversation });

	

	App.Router = new (Backbone.Router.extend({
		routes: { 
			"": "index", 
			"my-account": "account", 
			"conversations": "conversations", 
			"conversations/new": "newConversation", 
			"conversations/:id": "viewConversation",
			"*path": 'notFound'
		},

		index: function () {
			$("#content").html("blah blah blah");
		},
		account: function () {
			$("#content").html("my account");	
		},
		conversations: function () {

		},
		newConversation: function() {

		},
		viewConversation: function(id) {

		},
		notFound: function () {

		}
	}))();

	App.render();
	App.start();


});